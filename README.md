# 云开发 

这是云开发的快速启动指引，其中演示了如何上手使用云开发的三大基础能力：

- 数据库：一个既可在小程序前端操作，也能在云函数中读写的 JSON 文档型数据库
- 文件存储：在小程序前端直接上传/下载云端文件，在云开发控制台可视化管理
- 云函数：在云端运行的代码，微信私有协议天然鉴权，开发者只需编写业务逻辑代码

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)


# 我和我的莹莹

## 介绍
个人日常使用小程序,目前仍在完善。
使用的微信小程序原生的云开发。
欢迎大家参与开发。
期待大家的建议。

博客地址：
[程序员的浪漫之——情侣日常小程序](https://blog.csdn.net/weixin_67445519/article/details/126649469)


## 软件架构
软件架构说明


## 安装教程
1.  创建数据库
2.  下载源码
3.  导入源码
4.  关联云空间环境
5.  上传云函数
6.  下载图片资源，并传到云空间
7.  运行代码

## 资源
[项目图片资源下载](https://gitee.com/gong-kangle/picture-resources.git)

### 准备

#### 工具

微信开发者工具

[https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)

#### 文档

微信开放文档

[https://developers.weixin.qq.com/miniprogram/dev/framework/](https://developers.weixin.qq.com/miniprogram/dev/framework/)

代码地址

https://gitee.com/gong-kangle/couple-applet.git

#### 账号

没有小程序账号的需要先去申请一个账号

官网地址：[https://mp.weixin.qq.com/wxamp/home/guide?lang=zh_CN&token=1210542861](https://mp.weixin.qq.com/wxamp/home/guide?lang=zh_CN&token=1210542861)

### 开发

#### 1、下载代码

将代码拉取到本地，然后导入微信开发者工具，需要登录并关联自己的云空间。

#### 2、设置账号信息
![输入图片说明](img/1.png)

因为是自用的，所以也就没有将账号信息存放到数据库中，而是保存为一个用户配置文件，文件位置如上图。userConfig对象中的key对应账号，password则是密码，nickName为在小程序中显示的昵称，eName是英文别名（这里是随便起的），color为代表色，icon是代表图表，wishIcon为许愿树模块中的礼物盒图标，将账号信息配置好即可登录小程序。

#### 3、上传图片并修改路径

图片包地址：[](https://gitee.com/gong-kangle/picture-resources.git)

因为一开始只是为了做一个记账本功能的小程序，所以我在云存储中创建了一个记账本文件夹，如下图：
![输入图片说明](img/2.png)
![输入图片说明](img/3.jpg)

我们可以在这里创建一个文件夹（或者自己重新命名创建一个文件夹，代码中对应的图片路径要改变），下载好图片包后上传到该目录中，然后修改小程序中对应的文件路径即可。图片的公共路径在配置文件我已经进行了抽取配置，这里修改为你云空间的存储路径即可，如下图：

![在这里插入图片描述](img/4.jpg)


#### 4、上传云函数

![输入图片说明](img/5.jpg)


右键选择上传并部署（云端安装依赖）

#### 5、初始化数据库

首次导入需要先初始化数据库，已集成为函数一键初始化，只需点击我的页面中的初始化数据库按钮即可。

![输入图片说明](img/%E6%88%91%E7%9A%84.png)


#### 6、上传代码为体验版

![输入图片说明](img/6.jpg)

上传之后代码可以在自己的小程序后台看到上传的版本，目前个人不支持上传为线上版本，所以我们只能使用体验版，设置为体验版的方法如下：
微信小程序管理后台地址：[https://mp.weixin.qq.com]([微信公众平台 (qq.com)](https://mp.weixin.qq.com/))

![输入图片说明](img/7.jpg)

#### 7、添加体验成员

上传为体验版之后，还应该要将对方设置为体验成员或项目成员，对方才有权限体验小程序，和你一起使用小程序。
![在这里插入图片描述](img/8.jpg)


### 体验

点击体验版二维码这里，就会弹出一个二维码，扫码后即可打开小程序，然后将其分享给你的女朋友即可一起使用该小程序了。

![在这里插入图片描述](img/9.jpg)



### 一起开发
目前项目已全部开源，小程序样式相对来说还是不够美观，后续还会继续优化并开发完善新旧功能，有兴趣一起维护开发的可以一起。

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

## 需求定制
承接定制化功能需求开发，有需要定制的需求功能可以联系我。

## 联系我
1. 个人微信：wg_925
2. qq:25312913

## 赞赏
如果觉得该小程序对您可以有一点的帮助，可以在这里请作者喝一瓶可乐。

![输入图片说明](img/IMG_20220901_203108.jpg)

